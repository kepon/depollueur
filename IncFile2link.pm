package File2link;
# https://formation-perl.fr/guide-perl-10.html
# https://www.tutorialspoint.com/sqlite/sqlite_perl.htm#
use strict;
use warnings;
use DBI;
use Data::Dumper;
use Digest::MD5::File qw( file_md5_hex );
use Switch;
use Time::Local 'timelocal_nocheck';
use File::Copy;

our $id=int(rand(99));

sub periode2seconde {
    my ($periode) = @_;
    switch($periode) {
        case "minute"    { return 60 }
        case "hour"      { return 3600 }
        case "day"       { return 86400 }
        case "week"      { return 604800 }
        case "month"     { return 2592000 }
        case "quarter"   { return 7776000 }
        else             { return 7776000 }
    }
}
sub upload {
   my ($attach_name, $attach_media, $periode) = @_;
   ### récupération date courante
   my ($sec, $min, $hour, $day, $month, $year) = localtime();
   ### reconstruction date voulue (en ajoutant les mois passés en paramètre)
   my $attach_periode = timelocal_nocheck($sec, $min, $hour, $day, $month, $year)+periode2seconde($periode);
   my $upload_url = $main::config->{f2l_url}."/".$attach_periode.'-'.$id.'/'.$attach_name;
   &main::logEntry(3, $$main::color{'GREEN'}."URL : ".$upload_url);
   my $attach_md5 = file_md5_hex( $attach_media );
   &main::logEntry(4, "attach_md5: ".$attach_md5);
   my $checkChecksum = checkChecksum($attach_md5);
   if (ref($checkChecksum) eq 'HASH') {
      &main::logEntry(1, "CheckChecksum: error ".Dumper($checkChecksum));
      return 0;
   } elsif ($checkChecksum eq 0 || -e $main::config->{f2l_dir}."/".$checkChecksum) {
      # Mkdir
      my $upload_dir = $main::config->{f2l_dir}."/".$main::config->{f2l_files}."/".$attach_periode.'-'.$id;
      &main::logEntry(5, "mkdir ".$upload_dir);
      mkdir $upload_dir, 0755;
      chmod 0755, $upload_dir;
      chown $main::config->{f2l_upload_uid}, $main::config->{f2l_upload_gid}, $upload_dir;
      if (-e $main::config->{f2l_dir}."/".$checkChecksum) {
         &main::logEntry(4, "CheckChecksum: $checkChecksum - pièce jointe existe ! ");
         # Link
         &main::logEntry(3, "Link file ".$main::config->{f2l_dir}."/".$checkChecksum." to $upload_dir/$attach_name");
         my $retval = link $main::config->{f2l_dir}."/".$checkChecksum, $upload_dir.'/'.$attach_name ;
         if( $retval != 1 ) {
            &main::logEntry(1, "Link error : ".$!);
            return 0;
         }
      } else {
         &main::logEntry(4, "CheckChecksum: 0 - la pièce jointe n'existe pas, on l'upload");
         # Copy
         &main::logEntry(3, "Copy file $attach_media to $upload_dir/$attach_name");
         my $retval = copy($attach_media, $upload_dir.'/'.$attach_name);
         if( $retval != 1 ) {
            &main::logEntry(1, "Copy error : ".$!);
            return 0;
         }
         chmod 0664, $upload_dir.'/'.$attach_name;
         chown $main::config->{f2l_upload_uid}, $main::config->{f2l_upload_gid}, $upload_dir.'/'.$attach_name;
      }
      # Checksum in database
      &main::logEntry(5, "Add checksum in database");
      File2link::addFile( $main::config->{f2l_files}."/".$attach_periode.'-'.$id.'/'.$attach_name, $attach_md5, $attach_periode );
      # Return URL
      return $upload_url;
   } else {
      &main::logEntry(1, "Erreur sur le retour de checkChecksum ".$checkChecksum);
      return 0
   }
}
sub archive {
   my (@attach_tmp_name, $periode) = @_;
   ### récupération date courante
   my ($sec, $min, $hour, $day, $month, $year) = localtime();
   ### reconstruction date voulue (en ajoutant les mois passés en paramètre)
   my $attach_periode = timelocal_nocheck($sec, $min, $hour, $day, $month, $year)+periode2seconde($periode);
   my $url = $main::config->{f2l_url}."/".$attach_periode.'-'.$id.'.zip';
   &main::logEntry(3, "Archive ".$url);
   return $url;
}
sub dbConnect() {
   my $driver   = "SQLite"; 
   my $database = $main::config->{f2l_db};
   my $dsn = "DBI:$driver:dbname=$database";
   my $userid = "";
   my $password = "";
   eval {
      my $dbh = DBI->connect($dsn, $userid, $password, { RaiseError => 1 }) 
         or die $DBI::errstr;
      return $dbh;
   };
}
sub addFile {
   my ($file_path, $file_md5sum, $dateExpir) = @_;
   &main::logEntry(5, "[sqlite] INSERT $file_path, $file_md5sum, $dateExpir");
   my $dbh = dbConnect();
   my $stmt = qq(INSERT INTO checksum (file_path,file_md5sum,dateExpir)
                  VALUES ('$file_path', '$file_md5sum', '$dateExpir'));
   eval {
      my $rv = $dbh->do($stmt) ;
   } or do {
      &main::logEntry(1, "addFile : ".$DBI::errstr);
      return $DBI::errstr;
   };
   $dbh->disconnect();
   return 1;
}
# Return 
# ref(return) : HASH = erreur
# return : 0 : n'existe pas
# autre : le nom du file_type
sub checkChecksum {
   my ($checksum) = @_;
   #print "Bonjour $checksum \n";
   #print Dumper(@ISA);
   #print Dumper($main::config);
   &main::logEntry(5, "[sqlite] SELECT $checksum");
   my $dbh = dbConnect();
   my $stmt = qq(SELECT id, file_path, dateExpir FROM checksum WHERE file_md5sum = "$checksum";);
   my $sth;
   eval {
      $sth = $dbh->prepare( $stmt );
      my $rv = $sth->execute() ;
   } or do {
      &main::logEntry(1, "checkChecksum : ".$DBI::errstr);
      return {'error', $DBI::errstr};
   };
   my $row_count = 0;
   my $return;
   while(my @row = $sth->fetchrow_array()) {
         #print "id = ". $row[0] . "\n";
         #print "file_path = ". $row[1] ."\n";
         $return = $row[1];
         #print "dateExpir = ". $row[2] ."\n";
         $row_count = $row_count+1;
   }
   #print "row count $row_count";
   $dbh->disconnect();
   if ($row_count eq 0) {
      &main::logEntry(5, "[checkChecksum] return 0");
      return 0;
   } else {
      &main::logEntry(5, "[checkChecksum] return ".$return);
      return $return;
   }
   #print "Operation done successfully\n";
}
1;
