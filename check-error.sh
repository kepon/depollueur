#!/bin/bash

# apt install logtail

error=`/usr/sbin/logtail -o /tmp/mail.log.offset /var/log/mail.log | grep "postfix/pipe" | grep -v "status=sent" `
if ! (($?)) ; then
    echo $error | mail -s "Depollueur.pl - filter alert" david@retzo.net
fi


