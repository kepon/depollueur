# Depollueur

**Dépollution des courriel par substitution des pièces jointes par un lien temporaire**

Fork de https://git.kaz.bzh/KAZ/depollueur de Kaz (by [François](https://git.kaz.bzh/francois)) sous licence [CeCILL-B](http://www.cecill.info)

Ce qui change : 

* Ré-écriture du filtre postfix en Perl
* Utilisation de file2link plutôt que Jyrapheau
* Utilisation (quasi) en l'état le programme [eMailShrinker](https://git.kaz.bzh/KAZ/depollueur/src/branch/master/src/cpp/eMailShrinker.cpp) du Dépollueur de François

## Pré-requis

Le dépollueur de François (de Kaz.bzh) : https://git.kaz.bzh/KAZ/depollueur#compilation (pour son programme eMailShrinker) ici installé dans /opt (modifier bin_shrink du config.yml) (dépendance manquante : apg)

```
cd /opt
git clone https://git.kaz.bzh/KAZ/depollueur.git
cd /opt/depollueur
# lire le readme pour l'installation...
```

[File2Link](https://framagit.org/kepon/file2link) (v1.1+)  d'installer sur **le même serveur**, fonctionnel,  avec le mode "check_checksum" d'activé (true)

Dépendance Perl
* YAML::XS
* Capture::Tiny
* DBI
* Digest::MD5::File

Pour Debian : 

```bash
apt install libdigest-md5-file-perl libyaml-perl libyaml-libyaml-perl libswitch-perl   libdbi-perl libtext-unidecode-perl
```

## Installation 

### Sur ISPconfig

L'utilisateur avec lequel est mis en ligne  File2Link est l'utilisateur `web230` dans l'exemple :

```bash
mkdir /home/filter
chown web230 /home/filter
cd /home/filter
git clone https://framagit.org/kepon/depollueur.git . 
cp config-default.yml config.yml
mkdir /var/spool/filter
chown web230 /var/spool/filter
```

Modification du fichier /etc/postfix/master.cf :

```diff
smtp      inet  n       -       y       -       -       smtpd
+  -o content_filter=filter:dummy
[...]
submission inet n       -       y       -       -       smtpd
+  -o content_filter=filter:dummy
[...]
smtps     inet  n       -       y       -       -       smtpd
+  -o content_filter=filter:dummy
[...]
+# A la fin :
+# PIPE argument : http://www.postfix.org/pipe.8.html
+filter    unix  -       n       n       -       10      pipe
+    flags=Rq user=web230 null_sender=
+    directory=/home/filter
+    argv=perl /home/filter/depollueurFilter.pl -s ${size} -c /home/filter/config.yml -f ${sender} -- ${recipient} 
```

Ajouter un alias dans /etc/alias : 

```
depolluer: /dev/null
nepasdepolluer: /dev/null
```

Créer 2 "e-mail de transfère" via ISPconfig  : 

```
depolluer@vous.fr > depolluer@votrehostname.fr
nepasdepolluer@vous.fr > nepasdepolluer@votrehostname.fr
```

Application : 

```bash
newaliases
service postfix restart
```

### Autres

On part du principe que File2Link est exécuté en tant que www-data :

```bash
mkdir /home/filter
cd /home/filter
git clone https://framagit.org/kepon/depollueur.git . 
cp config-default.yml config.yml
mkdir /var/spool/filter
chown www-data /var/spool/filter
```

Modification du fichier /etc/postfix/master.cf :

```diff
smtp      inet  n       -       y       -       -       smtpd
+  -o content_filter=depollueur:dummy
[...]
submission inet n       -       y       -       -       smtpd
+  -o content_filter=depollueur:dummy
[...]
smtps     inet  n       -       y       -       -       smtpd
+  -o content_filter=depollueur:dummy
[...]
+# A la fin :
+# PIPE argument : http://www.postfix.org/pipe.8.html
+depollueur    unix  -       n       n       -       10      pipe
+    flags=Rq user=web230 null_sender=
+    directory=/home/filter
+    argv=perl /home/filter/depollueurFilter.pl -s ${size} -c /home/filter/config.yml -f ${sender} -- ${recipient} 
```

Ajouter un alias dans /etc/alias : 

```
depolluer: /dev/null
nepasdepolluer: /dev/null
```

Application : 

```bash
newaliases
service postfix restart
```

## Patch eMailShrinker

```bash
cd /opt/depollueur
patch /opt/depollueur/src/cpp/MainAttachment.cpp < /home/filter/eMailShrinker.patch
patch /opt/depollueur/src/cpp/Attachment.cpp < /home/filter/Attachment.patch
# re-compile
make -j $(nproc)
```

### Upldate eMailShrinker

```bash
cd /opt/depollueur
git pull
patch /opt/depollueur/src/cpp/MainAttachment.cpp < /home/filter/eMailShrinker.patch
patch /opt/depollueur/src/cpp/Attachment.cpp < /home/filter/Attachment.patch
# re-compile
make -j $(nproc)
```

## Configuration

La configuration se fait dans le fichier config.yml

```yml
f2l_db: "/mnt/f2l/files/checksum.db"
f2l_url: "https://dl.retzo.net/"
f2l_dir: "/var/www/dl.retzo.net/web"
f2l_upload_uid: 33  # Web f2L UID
f2l_upload_gid: 33  # Web f2L GID
```

L'application du filtre se fait par regex (apply_*) et par la taille du fichier max_keep_in_mail)

Voici quelques exemples pour mieux comprendre : 

### Tout avec exception

Configuration pour tout dépolluer, sauf : 
 * Si l'adresse de FROM ou de TO est root@* ou www-data@*
 * Si l'utilisateur à mis en BCC l'adresse nepasdeepolluer@mondomaine.com
 * Si l'utilisateur à mis dans le sujet tag "NEPASDEPOLLUER" (qui sera supprimé)

```yml
apply_to : ".+"
apply_from: ".+"
apply_to_exclude : "^root@|^www-data@"
apply_from_exclude : "^root@|^www-data@"
apply_bcc_exclude : "^nepasdeepolluer@" 
apply_subject_tag_exclude : "NEPASDEPOLLUER" 
apply_bcc : "^$" 
apply_subject_tag : "^$" 
```

### Uniquement certains domaines + sur demande

Configuration pour dépolluer uniquement si : 
 * Si l'adresse de FROM se termine en @domain1.com ou @domain2.com
  * Sauf si l'adresse de FROM est root@* ou www-data@* 
  * Sauf si l'adresse de TO est michel@domain1.com
 * Si l'utilisateur à mis en BCC l'adresse depolluer@mondomaine.com ou depollueur@mondomaine.com 
 * Si l'utilisateur à mis dans le sujet tag "NEPASDEPOLLUER" (qui sera supprimé)

```yml
apply_to : ""
apply_from: "@domain1.com$|@domaine2.com$"
apply_to_exclude : "michel@domain1.com"
apply_from_exclude : "^root@|^www-data@"
apply_bcc : "^depolluer@|^depollueur@" 
apply_subject_tag : "DEPOLLUER" 
apply_bcc_exclude : "^$" 
apply_subject_tag_exclude : "^$" 
```

## Environement de test


```bash
su - web230 
cd /home/filter
perl depollueurFilter.pl -f "david@mercereau.info" -- "david.mercereau@retzien.fr depolluer@retzien.fr" < msgtest/htmltag.orig 
```

## Changelog

* futur :
    * URL refresh (ne fonctionne pas non plus dans archive/filterRetzien.sh)  #1
* v0.2

## Licence

CeCILL-B (Look LICENCE.md)
