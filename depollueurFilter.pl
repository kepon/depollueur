#!/usr/bin/perl

# Sur une idée original de François / kaz.bzh
# Modifié par David Mercereau https://david.mercereau.info/contact/
# Licence CeCILL-B

use strict;
use warnings;
# Include
use lib '.';
# --------------- File2link ----------
use IncFile2link;
# / -----------------------------------
# yaml parse
use YAML::XS 'LoadFile';
# get paramètre
use Getopt::Long;
# Remove en recursif
use File::Path qw(rmtree);
# Capture stdout
use Capture::Tiny 'capture';
# Debug
use Data::Dumper;
# Charset convert
use Text::Unidecode;

# Version du programme
my $version = '0.2'; my $dateVersion = 'Mai 2023';

# Parametres
my $params = {
    'version'        => $version,
    'dateversion'    => $dateVersion,
    'programme'      => 'depollueurFilter',
    'help'           => 0,
    'config'         => './config.yml',
    'size'           => 0,
    'from'           => undef,
};

# Lecture des recipients
my @recipients;
my $recipients = '';
my $recipients_delimiter=0;
foreach (@ARGV) {
    if ($recipients_delimiter == 1) {
        push(@recipients, $_);
        $recipients = $recipients.' \''.$_.'\'';
    }
    if ($_ eq '--') {
        $recipients_delimiter=1;
    }
}
# Lecture des options ligne de commande
GetOptions(
    'help!'       => \$params->{help},
    'config:s'    => \$params->{config},
    'size:n'      => \$params->{size},
    'from:s'      => \$params->{from},
);

if ($params->{help} > 0) {
    print <<TEXTHELP;
Programme : $params->{programme}.pl V$params->{version} - ($params->{dateversion})
    Test un serveur SMTP en envoyant un ping\@hostname.
    Ce script vous répondra pong.
    
Perl version : $]

Usage : $params->{programme}.pl [Option ...] -- recipient1 recipient2

  Option :
    -help                       : Afficher l'aide
    -config=                    : Chemin du fichier de configuration
    -size=                      : Taille de l'e-mail
    -from=                      : sender

TEXTHELP
    exit();
}

#### Global
our $config = LoadFile($params->{config});
our $color = {
    5      => '[0m',
    4      => '[0;36m',
    3      => '[0;33m',
    2      => '[0;35m',
    1      => '[0;31m',
    'BOLD'   => '[1m',
    'RED'    => '[0;31m',
    'GREEN'  => '[0;32m',
    'YELLOW' => '[0;33m',
    'BLUE'   => '[0;34m',
    'MAGENTA' => '[0;35m',
    'CYAN'   => '[0;36m',
    'NC'     => '[0m',
    'NL'     => '',
};



#### Function
# Logfile
sub logEntry {
    # 5 debug, 4 info, 3 warning, 2 error, 1 cricical, 0 no
    my ($logLevel, $logText) = @_;
    my ( $sec, $min, $hour, $mday, $mon, $year, $wday, $yday, $isdst ) = localtime(time);
    my $dateTime = sprintf "%4d-%02d-%02d %02d:%02d:%02d", $year + 1900, $mon + 1, $mday, $hour, $min, $sec;
    
    if ($config->{log_level} >= $logLevel) {
        open(FH, '>>', $config->{log_path}) or die $!;
        print FH $$color{'NC'}."[".$dateTime."] ".$$color{$logLevel}.$logText.$$color{'NC'}."\n";
        close(FH);
    }
}
# Envoyer l'e-mail
sub sendmail {
    my ($msg_file) = @_;
    my $sendmail_cmd=$config->{bin_sendmail}." -f $params->{from} -- $recipients < $msg_file";
    logEntry(5, "exec :  $sendmail_cmd");
    my $sendmail_rc = system($sendmail_cmd);
    if ($sendmail_rc != 0) {
        logEntry(1, "Erreur l'exécussion de $sendmail_cmd : ".$!);
        quitFilter(100);
    } else {
        logEntry(3, $$color{'GREEN'}."E-mail envoyé ! ");
    }
    return $sendmail_rc;
}
# Sortie
sub quitFilter {
    my ($exit_code) = @_;
    if ($exit_code == 0) {
        logEntry(3, "Sortie : (exit_code:$exit_code) ");
    } else {
        logEntry(1, "Sortie : (exit_code:$exit_code) ");
    }
    exit $exit_code;
}
# Conversion des nom de fichier (retirer les caractère spéciaux)
sub stripFileName {
    my ($string) = @_;
    $string =~ tr/ÀÁÂÃÄÅÇÈÉÊËÌÍÎÏÑÒÓÔÕÖÙÚÛÜÝàáâãäåçèéêëìíîïñòóôõöùúûüýÿ/AAAAAACEEEEIIIINOOOOOUUUUYaaaaaaceeeeiiiinooooouuuuyy/;
    $string = unidecode( $string );
    $string =~ s/\n//g;
    $string =~ s/\s/_/g;
    $string =~ s/[^-\w .]//g;
    return $string;
}

logEntry(3, "Lancement du script PID:$$");

logEntry(5, "Debug params".Dumper($params));

#### Vérificatoin des paramètres : 
logEntry(4, "Param : from = ".$params->{from});
foreach ( @recipients ) {
    logEntry(4, "Param : recipient = ".$_);
}
if ($params->{size} != 0) {
    logEntry(4, "Param : size = ".$params->{size});
} else {
    logEntry(4, "Param : size = ?");
}

logEntry(4, "Config : default_mode = ".$config->{default_mode});
logEntry(4, "Config : default_periode = ".$config->{default_periode});
my $mode = $config->{default_mode};
my $periode = $config->{default_periode};

#### Préparation des répertoires
if (! -e $config->{tmp_path}) {
    if (!mkdir($config->{tmp_path})) {
        logEntry(1, "Imposible de créer ".$config->{tmp_path});
        quitFilter(254);
    }
} 
if (! -e $config->{ispect_dir}) {
    if (!mkdir($config->{ispect_dir})) {
        logEntry(1, "Imposible de créer ".$config->{ispect_dir});
        quitFilter(252);
    }
} 
my $date_temps = time();
my $rep_piece_jointe = $config->{tmp_path}."/".$date_temps."-$$";
if (!mkdir($rep_piece_jointe)) {
    logEntry(1, "Imposible de créer ".$rep_piece_jointe);
    quitFilter(253);
} else {
    logEntry(5, "Rep pièce jointe : ".$rep_piece_jointe);
}

#### Préparation des variables :
my $msg_file = $config->{ispect_dir}."/in.$$";
my $msg_file_altered = $msg_file.".altered";
my $apply_subject_tag_match = 0;
my $apply_subject_tag_exclude_match = 0;
my $subject_regex = qr/^Subject:/mp;
my $subject;
#### Enregistrement du message
logEntry(4, "Enregistrement du message : ".$msg_file);
open(FH, '>',$msg_file) or die logEntry(1, "Erreur à l'enregistrement du message : ".$!);
foreach my $line ( <STDIN> ) {
    ## Traitement du message avant enregistrement
    # Détecter le sujet (pour le futur)
    if (! defined $subject && $line =~ /$subject_regex/g ) {
        $subject = $line;   
        # Recherche des tags dans le sujet + suppression (si jamais)
        my $apply_subject_tag_regex = qr/$config->{apply_subject_tag}/mp;
        if ($subject =~ qr/$apply_subject_tag_regex/mp ) {
            # Match
            $apply_subject_tag_match = 1;
            # Suppression du mail
            my $apply_subject_tag_regex_subst = qr/$config->{apply_subject_tag}/mp;
            $line = $line =~ s/$apply_subject_tag_regex_subst//rg;
        }
        my $apply_subject_tag_exclude_regex = qr/$config->{apply_subject_tag_exclude}/mp;
        if ($subject =~ qr/$apply_subject_tag_exclude_regex/mp ) {
            # Match
            $apply_subject_tag_exclude_match = 1;
            # Suppression du mail
            my $apply_subject_tag_exclude_regex_subst = qr/$config->{apply_subject_tag_exclude}/mp;
            $line = $line =~ s/$apply_subject_tag_exclude_regex_subst//rg;
        }
    }
    #chomp( $line );
    print FH "$line";
}
close(FH);
if (! defined $subject) {
    $subject = ' ';
}
logEntry(4, "Sujet déterminé : ".$subject);
logEntry(5, "apply_subject_tag_match : ".$apply_subject_tag_match);
logEntry(5, "apply_subject_tag_exclude_match : ".$apply_subject_tag_exclude_match);

#### Conversion dos2unix
my $dos2unix_cmd=$config->{bin_dos2unix}." '$msg_file' 2> /dev/null";
logEntry(5, "exec : ".$dos2unix_cmd);
my $dos2unix_rc = system($dos2unix_cmd);
if ($dos2unix_rc != 0) {
    logEntry(1, "Erreur l'exécussion de $dos2unix_cmd : ".$!);
}

#### Si le "size" n'est pas passé en paramètre, on le recherche
if ($params->{size} == 0) {
    $params->{size} = -s $msg_file;
    logEntry(4, "Param : size (find) = ".$params->{size});
}
print "depollueur|".$params->{size};

### Si le "size" est plus petit que max_keep_in_mail
if ($params->{size} < $config->{max_keep_in_mail}) {
    logEntry(3, "L'e-mail est petit, pas de dépollution");
    my $sendmail_rc = sendmail($msg_file);
    if ($config->{debug} != 1) {
        unlink $msg_file;
        rmtree $rep_piece_jointe;
    }
    quitFilter($sendmail_rc);
}

## FIND : FROM
my $apply_from_match = 0;
my $apply_from_exclude_match = 0;
# Ignorer le filtre si le FROM est en liste blanche (apply_from_exclude) - regex
if ($params->{from} =~ qr/$config->{apply_from}/mp ) {
    logEntry(3, "Apply_from match : ".$params->{from}." / ".$config->{apply_from});
    $apply_from_match = 1;
}
if ($params->{from} =~ qr/$config->{apply_from_exclude}/mp ) {
    logEntry(3, "Apply_from exclude match : ".$params->{from}." / ".$config->{apply_from_exclude});
    $apply_from_exclude_match = 1;
}
## FIND : TO / BCC
my $apply_to_match = 0;
my $apply_to_exclude_match = 0;
my $apply_bcc_match = 0;
my $apply_bcc_exclude_match = 0;
foreach ( @recipients ) {
    if ( $_ =~ qr/$config->{apply_to}/mp ) {
        logEntry(3, "Apply_to match : $_ / ".$config->{apply_to});
        $apply_to_match = 1;
    } 
    if ( $_ =~ qr/$config->{apply_to_exclude}/mp ) {
        logEntry(3, "Apply_to exclude match : $_ / ".$config->{apply_to_exclude});
        $apply_to_exclude_match = 1;
    }
    if ( $_ =~ qr/$config->{apply_bcc}/mp ) {
        $apply_bcc_match = 1;
        logEntry(3, "Apply bcc match : $_  / ".$config->{apply_bcc});
    } 
    if ( $_ =~ qr/$config->{apply_bcc_exclude}/mp ) {
        $apply_bcc_exclude_match = 1;
        logEntry(3, "Apply bcc exclude match : $_  / ".$config->{apply_bcc_exclude});
    }
}
# APPLY FILTER
my $depollution = 0;
if ($apply_to_match == 1 || $apply_from_match == 1) {
    $depollution = 1;
    logEntry(4, "apply to|from=true, DEPOLLUTION !");
} else {
    logEntry(4, "apply to|from=false, pas de depollution");
}
if ($apply_from_exclude_match == 1 || $apply_to_exclude_match == 1) {
    $depollution = 0;
    logEntry(4, "apply to|from exclude=true, pas de depollution !");
}
if ($apply_bcc_match == 1 || $apply_subject_tag_match == 1) {
    $depollution = 1;
    logEntry(3, "Dépolluttion explicitement demandé (bcc|subject)");
}
if ($apply_bcc_exclude_match == 1 || $apply_subject_tag_exclude_match == 1) {
    $depollution = 0;
    logEntry(3, "Dépolluttion explicitement refusé (bcc|subject)");
}
if ($depollution == 0) {
    my $sendmail_rc = sendmail($msg_file);
    logEntry(3, $$color{'GREEN'}."E-mail non dépollué :(, envoi du message original");
    quitFilter($sendmail_rc);
} else {
    logEntry(3, $$color{'GREEN'}."Dépollution... "); 
}

### Etape extraction des liens / refresh - non fonctionnelle
# my $shrink_cmd_a=$config->{bin_shrink}." '".$msg_file."' > /tmp/test-url-list";
# logEntry(5, "exec : ".$shrink_cmd_a);
# my ($shrink_a_out, $shrink_a_err, $shrink_a_rc) = capture {
#     system($shrink_cmd_a);
# };
# if ($shrink_b_rc != 0) {
#     logEntry(1, "Erreur l'exécussion de $shrink_cmd_b : ".$!);
#     my $sendmail_rc = sendmail($msg_file);
#     if ($config->{debug} != 1) {
#         unlink $msg_file;
#         rmtree $rep_piece_jointe;
#     }
#     quitFilter($sendmail_rc);
# } else {
#     logEntry(5, "stdout:\n".$$color{'BLUE'}.$shrink_b_out);
# }

### Etape extraction des pieces jointes
my $shrink_cmd_b=$config->{bin_shrink}." -s '".$config->{max_keep_in_mail}."' -d '".$rep_piece_jointe."' '".$msg_file."'  2>> ".$rep_piece_jointe."/log";
logEntry(5, "exec : ".$shrink_cmd_b);
my ($shrink_b_out, $shrink_b_err, $shrink_b_rc) = capture {
  system($shrink_cmd_b);
};
if ($shrink_b_rc != 0) {
    logEntry(1, "Erreur l'exécussion de $shrink_cmd_b : ".$!);
    my $sendmail_rc = sendmail($msg_file);
    if ($config->{debug} != 1) {
        unlink $msg_file;
        rmtree $rep_piece_jointe;
    }
    quitFilter($sendmail_rc);
} else {
    logEntry(5, "stdout:\n".$$color{'BLUE'}.$shrink_b_out);
}
# Traitement des pièces jointes
my @attach_tmp_name = split /\n/, $shrink_b_out;
my $nb_attach = scalar @attach_tmp_name;
my $attach_error = 0;
# écriture d'un fichier url-list à piper dans eMailShrinker
#   arch: none (si un seul fichier)|url|bad
#   url: url (autant que de fichier)
open(UL, '>', $config->{tmp_path}.'/url-list');
logEntry(3, "Nombre de pièce jointe: ".$nb_attach);
foreach ( @attach_tmp_name ) {
    logEntry(5, $$color{'BLUE'}."--     attach_tmp_name: ".$_);
    my $attach_media = $_."/media";
    logEntry(5, "attach_media: ".$attach_media);
    my $attach_meta = $_."/meta";
    my $name_regex = qr/^Name: /mp;
    my $attach_name;
    open(FH, '<:encoding(utf8)', $attach_meta) or die $!;
    while(<FH>){
        if ($_ =~ /$name_regex/g ) {
            $attach_name = $_ =~ s/$name_regex//rg;
            $attach_name = stripFileName($attach_name);
        }
    }
    close(FH);
    logEntry(5, "attach_name: ".$attach_name);
    logEntry(3, "Upload : ".$attach_name);
    my $upload_return = File2link::upload($attach_name, $attach_media, $periode);
    if ($upload_return eq 0) {
        $attach_error = $attach_error +1;
    } else {
        print UL 'url: '.$upload_return."\n";
    }
}
# Ajout de l'archive 
if ($nb_attach > 1) {
    my @attach_tmp_name = split /\n/, $shrink_b_out;
    print UL 'arch: '.File2link::archive(@attach_tmp_name, $periode);
} else {
    print UL 'arch: none';
}
close(UL);
# S'il y a eu des erreur d'upload, on envoi le message non modifié !
if ($attach_error > 0 || $nb_attach == 0) {
    if ($attach_error > 0) {
        logEntry(1, "Des erreurs, on envoi le message sans le modifier");
    } elsif ($nb_attach == 0) {
        logEntry(4, "Envoi du message original, il n'y a pas de pièce jointe...");
    }
    my $sendmail_rc = sendmail($msg_file);
    if ($config->{debug} != 1) {
        unlink $msg_file;
        rmtree $rep_piece_jointe;
    }
    quitFilter($sendmail_rc);
}

### Etape de création du nouveau message
my $shrink_cmd_c=$config->{bin_shrink}." -s '".$config->{max_keep_in_mail}."' -m $mode $msg_file $msg_file_altered < ".$config->{tmp_path}.'/url-list';
logEntry(5, "exec : ".$shrink_cmd_c);
my ($shrink_c_out, $shrink_c_err, $shrink_c_rc) = capture {
  system($shrink_cmd_c);
};

my $sendmail_rc = 0;
if ($shrink_c_rc != 0) {
    logEntry(1, "Erreur l'exécussion de $shrink_cmd_c : ".$!);
    # Envoi du message original
    $sendmail_rc = sendmail($msg_file);
} else {
    my $size_altered = -s $msg_file_altered;
    print "|$size_altered";
    # Envoi du message altered
    logEntry(5, "stdout:".$$color{'BLUE'}.$shrink_c_out);
    logEntry(3, $$color{'GREEN'}."E-mail altered sent ($msg_file_altered) size altered: $size_altered");
    $sendmail_rc = sendmail($msg_file_altered);
}

### END

# Ménage
if ($config->{debug} == 0) {
    logEntry(5, "Ménage dans les différents fichiers temporaire:".$$color{'BLUE'}.$shrink_c_out);
    rmtree $rep_piece_jointe;
    unlink $msg_file;
    unlink $msg_file_altered    ;
}
quitFilter($sendmail_rc);

